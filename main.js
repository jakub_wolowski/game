'use strict';
//get canvas and prepare for painting
const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
canvas.width = 360;
canvas.height = 400;
const cw = canvas.width;
const ch = canvas.height;

//load bullet
function makeBullet() {
    const bullet = new Image();
    bullet.addEventListener('load'), function loadBullet() {
        ctx.drawImage(bullet, 10,10,20,20);
    };
    bullet.src = 'bullet.jpg';
}

function drawMap(){
    const map = new Image();
    map.addEventListener('load', function loadMap() {
        ctx.drawImage(map, 0, 0, cw, ch);
    });
    map.src = 'map.png';
}

// constructor of all npc like hero, enemy
class npc {
    constructor(x,y,offsetX,offsetY,hp) {
        this.x = x,
        this.y = y,
        // this.offsetX = offsetX,
        // this.offsetY = offsetY,
        this.hp = hp;
    }
     //physic of hero moving
        move() {
            window.addEventListener("keydown", (event) => {
                if (event.defaultPrevented) {
                    return; // Do nothing if the event was already processed
                }
                do {
                    npc.getBoundingClientRect();
                } while (event.key);

                //key control prepare for keydown event wasd to move
                const wKey = String.fromCharCode(87).toLocaleLowerCase();
                const aKey = String.fromCharCode(65).toLocaleLowerCase();
                const sKey = String.fromCharCode(83).toLocaleLowerCase();
                const dKey = String.fromCharCode(68).toLocaleLowerCase();
                const enter = String.fromCharCode(13);

                switch (event.key) {
                    case 83:
                        hero.y -= 10;
                        break;
                    case 87:
                        hero.y += 10;
                        break;
                    case 65:
                        hero.x -= 10;
                        break;
                    case 68:
                        hero.x += 10;
                        break;
                    case 13:
                        hero.shoot();
                        break;
                    default:
                        return; // Quit when this doesn't handle the key event.
                    }
                    // Cancel the default action to avoid it being handled twice
                    event.preventDefault();
                }, true);
            }

    //shoot physic
        shoot() {
            window.addEventListener("click", () => {
                const bullet = {
                    hp: 10,
                    bulletShoot() {
                            const bullet = new Image();
                            bullet.addEventListener('load'), function loadBullet(x,y,endX,endY) {
                                ctx.drawImage(bullet, x, y, endX, endY);
                            };
                            bullet.src = 'bullet.jpg';

                    }
                };
                if(hero.click){
                    bullet.bulletShoot();
                }
            });
        }
    }

//load hero image function
function makeHero() {
    const heroImage = new Image();
    heroImage.addEventListener('load', function loadHero() {
        ctx.drawImage(heroImage, 150, 150, 200, 200);
    });
    heroImage.src = 'hero.png';
}

//load enemy image function
function makeEnemy() {
    const enemyImage = new Image();
    enemyImage.addEventListener('load', function loadEnemy(x,y,endX,endY) {
        ctx.drawImage(enemyImage, x, y, endX, endY);
    });
    enemyImage.src = 'enemy.jpg';
}
//load big enemy function
function makeBigEnemy() {
    const bigEnemyImage = new Image();
    bigEnemyImage.addEventListener('load', function loadEnemy(x, y, endX, endY) {
        ctx.drawImage(bigEnemyImage, x, y, endX, endY);
    });
    bigEnemyImage.src = 'bigEnemy.jpg';
}

//create an hero and enemies
const hero = new npc(cw / 2 - 50, ch / 2 - 50, 100);
const enemy = new npc(Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), 10);
const bigEnemy = new npc(20);

//end game scenario
function endGame(){
    if(hero.enemyCounter() == 50){
        ctx.font = '36px serif';
        ctx.fillText('You\'re winner, just go to e-sport', cw / 2, ch / 2 - 100);
    } else {
        ctx.font = "36px serif";
        ctx.fillText("Game over, try again ! (reload by f5)", cw / 2, ch / 2 - 100);
    }
}
//counter for hero. If kill 50 enemy end the game
function enemyCounter() {
    for (let i = 0; i <= 50; i++)
        if (enemy.hp === 0 || bigEnemy.hp === 0) {
            i++;
            if (i == 50) {
                endGame();
            }
        }
}
//function which initialize the game
function init(){
    drawMap();
    makeHero();
    while(enemyCounter()){
        if (enemy < 5) {
            makeEnemy(Math.floor(Math.random() * 100), Math.floor(Math.random() * 100)); //make enemy 0-100,0-100 but how to do all map without center
        }
        if (bigEnemy < 1) {
            makeBigEnemy(Math.floor(Math.random() * 100), Math.floor(Math.random() * 100));
        }
    }
}
window.setInterval(init, 1000/ 60);